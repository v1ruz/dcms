import Vue from 'vue'
import Router from 'vue-router'
import { get } from 'lodash'
import { getLoggedinUser } from '@core/helpers'
import { PatientsHome, AddPatient } from '@pages/patients'
import {
  PageNotFound,
  Login,
  Register,
  Dashboard,
  Patients,
} from '../pages'

declare var $: any;

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Dashboard, name: 'dashboard' },
    { path: '/login', component: Login },
    { path: '/register', component: Register },
    {
      path: '/patients', component: Patients,
      children: [
        { path: '', component: PatientsHome },
        { path: 'new', component: AddPatient },
        { path: '*', redirect: { name: 'dashboard' } }
      ]
    },
    { path: '*', component: PageNotFound },
  ]
})

router.beforeEach((to, from, next) => {
  if (['/register', '/login'].indexOf(to.path) === -1) {
    const user = getLoggedinUser()
    if (!user) {
      return next('/login')
    }
  }
  next()
})

router.afterEach((to, from) => {
  if (get($.app, 'menu.expanded') === true && to.hash !== '#') {
    $.app.menu.toggle()
  }
})

export default router
