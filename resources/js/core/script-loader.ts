export default {
  load(attrs, el = document.body) {
    const script = document.createElement('script')
    script.type = 'text/javascript'

    if (attrs && Object.keys(attrs).length > 0) {
      for (const prop in attrs) {
        if (prop === 'text') {
          script.text = attrs[prop]
        } else {
          script.setAttribute(prop, attrs[prop])
        }
      }
    }

    el.appendChild(script)
  },

  loadModernScripts() {
    const scripts = [
      // '/app-assets/vendors/js/vendors.min.js',
      // '/app-assets/vendors/js/forms/icheck/icheck.min.js',
      // '/app-assets/vendors/js/tables/datatable/datatables.min.js',
      '/app-assets/vendors/js/charts/chart.min.js',
      '/app-assets/vendors/js/charts/echarts/echarts.js',
      '/app-assets/js/core/app-menu.min.js',
      '/app-assets/js/core/app.min.js',
      // '/app-assets/js/scripts/forms/form-login-register.js',
    ]
    scripts.forEach(script => {
      this.load({src: script})
    })
  }
}
