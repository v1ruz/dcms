import axios from 'axios'

interface CurrentUser {
  name: string,
  token: string
}

export function registerUser(credentials) {
  return new Promise((res, rej) => {
    axios.post('/api/auth/register', credentials)
    .then(response => {
      res(response.data);
    })
    .catch(err => {
      rej('An error occured.. try again later.')
    })
  })
}

export function login(credentials) {
  return new Promise((res, rej) => {
    axios.post('/api/auth/login', credentials)
    .then(response => {
      res(response.data);
    })
    .catch(err => {
      rej('Wrong Email/Password combination.')
    })
  })
}

export function getLoggedinUser(): CurrentUser {
  const userStr = localStorage.getItem('user');

  if(!userStr){
    return null
  }

  const user: CurrentUser = JSON.parse(userStr)

  axios.defaults.headers.common['Authorization'] = `Bearer ${user.token}`

  return user;
}
