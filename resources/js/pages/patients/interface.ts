interface Patient {
  firstname: string,
  middlename: string,
  lastname: string,
  address: string,
  email: string,
  contact: string,
  gender: 'Male' | 'Female',
  age: number,
  birthdate: string,
  birthplace: string,
  civilstatus: string,
  religion: string,
  nationality: string,
}

interface PatientList {
  name: string,
  disease: string,
  location: string,
  age: number,
  previousVisit: string,
  amountDue: number
}

export {
  Patient,
  PatientList
}