<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'middlename' => $faker->firstName[0],
        'lastname' => $faker->lastName,
        'address' => $faker->address,
        'email' => $faker->safeEmail,
        'contact' => $faker->phoneNumber,
        'gender' => $faker->numberBetween(1, 10) > 5 ? 'Male' : 'Female',
        'age' => $faker->numberBetween(20, 60),
        'birthdate' => '07-01-1990',
        'birthplace' => $faker->address,
        'civilstatus' => $faker->randomLetter,
        'religion' => $faker->randomLetter,
        'nationality' => $faker->randomLetter,
    ];
});
