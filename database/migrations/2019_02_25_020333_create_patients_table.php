<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('firstname');
            $table->string('middlename');
            $table->string('lastname');
            $table->text('address');
            $table->string('email');
            $table->string('contact');
            $table->enum('gender', ['Male', 'Female']);
            $table->integer('age');
            $table->string('birthdate');
            $table->text('birthplace');
            $table->string('civilstatus');
            $table->string('nationality');
            $table->string('religion');
            $table->text('photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
