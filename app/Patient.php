<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'address',
        'email', 'contact', 'gender', 'age', 'nationality',
        'birthdate', 'birthplace', 'civilstatus', 'religion',
    ];
}
