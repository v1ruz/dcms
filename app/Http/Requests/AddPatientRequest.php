<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string',
            'middlename' => 'required|string',
            'lastname' => 'required|string',
            'address' => 'required|string',
            'email' => 'required|email',
            'contact' => 'required|string',
            'gender' => 'required|in:Male,Female',
            'age' => 'required|numeric',
            'birthdate' => 'required|string',
            'birthplace' => 'required|string',
            // 'civilstatus' => 'required',
            // 'religion' => 'required',
        ];
    }
}
