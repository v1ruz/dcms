const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.ts('resources/js/app.ts', 'public/js').extract()
   .sass('resources/sass/app.scss', 'public/css')
   .copyDirectory('resources/app-assets', 'public/app-assets')
   .webpackConfig({
     resolve: {
       alias: {
         '@core': path.resolve('resources/js/core'),
         '@pages': path.resolve('resources/js/pages'),
         '@shared': path.resolve('resources/js/shared'),
         '@': path.resolve('resources')
       }
     }
   })
