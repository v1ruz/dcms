## Setup Vue Front-end

- nvm install && nvm use
- npm install
- npm run dev


## Setup JWT
- Follow instruction here: https://jwt-auth.readthedocs.io/en/develop/laravel-installation/


## Start the server

- php artisan key:generate
- php artisan migrate
- php artisan serve
